<?php

namespace App\Emr\OscarHost;

use Illuminate\Support\Facades\Log;
use Artisaninweb\SoapWrapper\SoapWrapper;
use Artisaninweb\SoapWrapper\Exceptions\ServiceAlreadyExists;
use Exception;
use SoapVar;
use SoapHeader;
use Validator;

class Client
{
	protected $soapWrapper;
	protected $baseUri;
	protected $oscarHostUsername;
	protected $oscarHostPassword;
	protected $securityData = [];
	protected $requestData = [];

	/*
		It may seem terrible, but it's easy enough.
		Main keys are all available services names.
		Second level is an array of available methods for the service.
		Third level is method's name, type and his parameters.
	 */
	protected $availableMethods = [
		'LoginService'       => [
			'login'
		],
		'DemographicService' => [
			[
				'name'       => 'getDemographic',
				'type'       => 'fetch',
				'parameters' => ['arg0']            
					// arg0 - demographicNo alias
			],
			[
				'name'			 => 'getDemographics',
				'type'			 => 'fetch_multiple',
				'parameters' => ['arg0','arg1','arg2'], 
					// arg0 - number of results to return per page (max 100)
					// arg1 - page number of results to return
					// arg2 - Earliest updated date to include
				'validation' =>	[
					'arg0'	=>	'required|integer|min:1|max:100',
					'arg1'	=>	'required|integer|min:1',
					'arg2'	=>	'required|date_format:Y-m-d'
				]	
			],
			[
				'name' => 'addDemographic'
			],
			[
				'name' => 'updateDemographic'
			],
			[
				'name' => 'getDemographicsByHealthNum'
			],
			[
				'name' => 'getDemographicByMyOscarUserName'
			]
		],
		'ProviderService'    => [
			[
				'name' => 'getProviders',
				'type' => 'fetch_multiple'
			]
		],
		'DrugService'        => [
			[
				'name' => 'getDrug',
				'type' => 'fetch',
				'parameters' => ['arg0']            // arg0 - drugNo alias
			],
			[
				'name'       => 'getDrugList',
			]
		],
		'EFormService'       => [
			[
				'name'       => 'getEForm',
				'parameters' => ['arg0']            // arg0 - ID alias
			],
			[
				'name' => 'getEFormData',
				'parameters'	=>	['arg0']					// arg0 - eform data id
			],
			[
				'name' => 'getEFormDataList',
				'parameters'	=>	["arg0","arg1"],
				// arg0 - eform id (oscarhost_eforms.oscr_id) 24 for med doc 
				// arg1 - demographicNo
				'validation' =>	[
					'arg0'	=>	'required|integer|min:1',
					'arg1'	=>	'required|integer|min:1',
				]
			],
			[
				'name' => 'getEFormDataListFiltered',
				'parameters'	=>	["arg0","arg1","arg2"],
				// arg0 - eform id (oscarhost_eforms.oscr_id) 24 for med doc 
				// arg1 - demographicNo
				// arg2 - array of key/values [ ['key' =>]	]
				'validation' =>	[
					'arg0'	=>	'required|integer|min:1',
					'arg1'	=>	'required|integer|min:1',
					'arg2'	=>	''
				]
			],
			[
				'name' => 'getEFormValue',
				'parameters'	=>	['arg0']
					// arg0 - id from getEformDataList
			],
			[
				'name' => 'getEFormList',
				
			],		
			[
				'name' => 'getEFormValueList',
				'parameters'	=>	['arg0']
					// arg0 - id from getEformDataList
			]
		]
	];

	function __construct(SoapWrapper $soapWrapper)
	{
		$this->soapWrapper = $soapWrapper;
		$this->baseUri = env('OSCARHOST_BASE_URI', 'xxx');
		$this->oscarHostUsername = env('OSCARHOST_API_USERNAME', 'yyy');
		$this->oscarHostPassword = env('OSCARHOST_API_PASSWORD', 'zzz');
	}

	/**
	 * Makes a call to the OscarHost API using the call method and returns response
	 * @param array $params         Array of parameters. If it's empty get parameters from $requestData
	 * @return array
	 * @throws Exception
	 */
	public function request(array $params = [])
	{
		if ($params) {
			$response_values = $this->call($params['service'], $params['method'], $params['query'], $params['auth']);
		} else {
			$response_values = $this->call($this->requestData['service'], $this->requestData['method'], $this->requestData['query']);
		}
		return $response_values;
	}

	/**
	 * Return SOAP API service name.
	 * @param string $resource      Resource type which was requested by an user (drug, demographic, etc)
	 * @return null|string          Name of service or null if it doesn't exist.
	 */
	public function getServiceFromResource(string $resource)
	{
		$service = null;
		switch ($resource) {
			case 'demographic':
				$service = 'DemographicService';
				break;
			case 'provider':
				$service = 'ProviderService';
				break;
			case 'drug':
				$service = 'DrugService';
				break;
			case 'eform':
				$service = 'EFormService';
				break;
			default:
				break;
		}
		return $service;
	}

	/**
	 * Look for a method by its service name and endpoint name
	 * @param  string $service      Must equal values of one of the keys in $this->availableMethods
	 * @param  string $endpointName Must equal the value of the "name" key in $this->availableMethods
	 * @return null|array           The endpoint array value for this service and endpoint name
	 */
	public function getMethod(string $service, string $endpointName)
	{
		$method = null;
		$available_methods = $this->availableMethods;
		foreach ($available_methods[$service] as $available_method) {
			if (isset($available_method['name']) && $available_method['name'] == $endpointName) {
				$method = $available_method;
				break;
			}
		}
		return $method;
	}

	/**
	 * Fill $requestData array. It is used in request() method.
	 * @param string $resource          Resource name (drug, for example). It is needed to load proper service.
	 * @param array $queryValues        Array with values which are used as parameters for request.
	 * @param string $endpointName     	Name endpoint of method. Must equal the value of the "name" key in $this->availableMethods
	 * @return $this                    Object of Client
	 * @throws Exception                If something is going wrong, return an Exception
	 */
	public function prepareRequest(string $resource, array $queryValues, string $endpointName = 'getEFormList')
	{
		$service = $this->getServiceFromResource($resource);
		if ($service === null) throw new Exception('Service is not available');

		$method = $this->getMethod($service, $endpointName);
		if ($method === null) throw new Exception('Method is not available');

		// try to combine $queryValues and $method['parameters'] without E_WARNING and throw an Exception if $method['parameters'] count isn't equal $queryValues count
		$queryParams = @array_combine($method['parameters'], $queryValues);
		
		if ($queryParams === false) {
			throw new Exception('Invalid number of query arguments for ' . $method['name']);
		}

		// Validate the data that we're sending based on the call
		if ( isset($method['validation']) && !empty($method['validation']) ) {
			$queryRules = $method['validation'];
			$validator = Validator::make($queryParams, $queryRules);

			if ( $validator->fails() ) {
				$_errorMessage = implode(' ', $validator->errors()->all() );
				throw new Exception($_errorMessage);
			}
		}

		if ( empty($queryParams) ) {
			$queryParams = [];
		}

		$this->requestData = [
			'service' => $service,
			'method' => $method['name'],
			'query' => $queryParams
		];

		return $this;
	}

	public function getLastResponse($serviceName = "EFormService")
	{
		try {
			$_client = $this->soapWrapper->client($serviceName, function($client) {
				return $client;
			});

			$response = $_client->getLastResponse();

			if ( env('APP_DEBUG') == true ) {
				Log::info($response);
			}

			return $response;
		} catch (Exception $e) {
			throw $e;
		}
	}

	public function getLastRequest($serviceName = "EFormService")
	{
		try {
			$_client = $this->soapWrapper->client($serviceName, function($data){ return $data; });

			$request = $_client->getLastRequest();

			if ( env('APP_DEBUG') == true ) {
				Log::info($request);
			}

			return $request;
		} catch (Exception $e) {
			throw $e;
		}
	}

	private function validateRequestParams(string $resource, array $queryValues, string $type = 'fetch')
	{
		$service = $this->getServiceFromResource($resource);
		if ($service === null) throw new Exception('Service is not available');

		$method = $this->getMethod($service, $type);
		if ($method === null) throw new Exception('Method is not available');
	}

	/**
	 * Make a request to the OscarHost API.
	 * @param string $service           Service name. Available services are listed as keys of $availableMethods property.
	 * @param string $method            Method name.
	 * @param array $queryData          Array with query data.
	 * @param bool $onlyAuthorize       Should we pass Username and Password to get response or not.
	 * @throws Exception                If something goes wrong throw an usual Exception.
	 * @return array
	 */
	private function call(string $service, string $method, array $queryData, bool $onlyAuthorize = true)
	{

		$securityData = $this->securityData;
		if ($onlyAuthorize && !$securityData && !$securityData = $this->setSecurityData()) {
			throw new Exception('Unable to login to Oscar API');
		}

		// Validate service and method as part of execution

		// Initialize Soap Wrapper
		$servicePath = $this->baseUri . $service . '?wsdl';
		try {
			$this->soapWrapper->add($service, function ($soapService) use ($method, $servicePath, $securityData) {
				$soapService->wsdl($servicePath);

				// If APP_DEBUG is true, set the trace property on the service
				if ( env('APP_DEBUG', false) ) {
					$soapService->trace(true);
				}

				// if user is logged we add Security header to the request
				if ($securityData) {
					$soapService->customHeader($this->createSecurityHeader());
				}
			});
		} catch (ServiceAlreadyExists $e) {
			//
		} catch (Exception $e) {
			throw new $e;
		}


		// Pull data
		$values = [];	
		$_call = $service.".".$method;
		$response = $this->soapWrapper->call($_call, [$queryData]);

		if (isset($response->return)) {
			$values = (array)$response->return;
		}

		return $values;
	}

	/**
	 * Look for security data in cache. If it exists set $securityData from cache
	 * or try to log into the OscarHost API if it does not
	 * @return bool                 Success of getting security data process.
	 * @throws Exception
	 */
	private function setSecurityData()
	{
		$securityData = cache('soap.security.data');

		if (empty($securityData) || empty($securityData['Username']) || empty($securityData['Password'])) 
		{
			// Try logging in a couple of times if you get an error
			try 
			{
				$success = $this->login();
				if ($success === true) 
				{
					$securityData = $this->securityData;
				}

			} catch (Exception $e) {
				throw $e;
			}
		} else {
			$this->securityData = $securityData;
		}
		return $securityData;
	}

	/**
	 * Try to log into the OscarHost API, set $securityData and put it on cache for 12 hours.
	 * @return bool Success (true) or fail (false).
	 */
	private function login()
	{
		try {
			
			// try logging in a couple of times
			// debugging a hunch
			$_count = 0;
			$response = $this->request([
				'service' => 'LoginService',
				'method' => 'login',
				'query' => [
					'arg0' => $this->oscarHostUsername, 'arg1' => $this->oscarHostPassword
				],
				'auth' => false]);

			if (!empty($response['securityId']) && !empty($response['securityTokenKey']))
			{
				break;
			}

			if (empty($response['securityId']) || empty($response['securityTokenKey']))
			{
				return false;
			}
			
			$securityData = [
				'Username' => $response['securityId'],
				'Password' => $response['securityTokenKey']
			];

			$this->securityData = $securityData;
			cache('soap.security.data', $securityData, 720);

		} catch (Exception $exception) {
			$this->securityData = false;
			throw $exception;
		}
		return true;
	}

	/**
	 * Return a SoapHeader object with wsse Security tag.
	 * @return SoapHeader
	 */
	private function createSecurityHeader()
	{
		$namespace = 'http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd';
		$mustUnderstand = true;

		// raw XML Security tag. It is the most easy and stable way to set up the header.
		$rawSecurityTag =   '<wsse:Security xmlns:wsse="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-secext-1.0.xsd">
         						<wsse:UsernameToken wsu:Id="UsernameToken-1" xmlns:wsu="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-wssecurity-utility-1.0.xsd">
            						<wsse:Username>' . $this->securityData['Username'] . '</wsse:Username>
            			            <wsse:Password Type="http://docs.oasis-open.org/wss/2004/01/oasis-200401-wss-username-token-profile-1.0#PasswordText">' . $this->securityData['Password'] . '</wsse:Password>
         			            </wsse:UsernameToken>
      			            </wsse:Security>';

		$securityTag = new SoapVar($rawSecurityTag, XSD_ANYXML);

		return new SoapHeader($namespace, 'Security', $securityTag, $mustUnderstand);
	}
}