<?php

namespace App\Emr\OscarHost;

use App\Emr\Factories\OscarHost\EloquentModelFactory;
use Artisaninweb\SoapWrapper\SoapWrapper;
use App\Emr\OscarHost\Client;
use App\Emr\Repositories\Eloquent\OscarHost\EformRepository;
use Illuminate\Support\Facades\Log;
use Exception;

class Sync
{

  public $client;
  
  function __construct()
  {
    try {
      $this->client = new Client( new SoapWrapper );
    } catch (Exception $e) {
      throw new $e;
    }
  }

  public function getOscrClient() 
  {
    $client = &$this->client;
    return $client;
  }

  public function fetchDemographicsByPage($pageNum = 1, $perPage = 100, $updatedSince = '2014-01-01')
  {
    // Start a client and do a pull
    $client = $this->getOscrClient();

    // Request data from the OscarHost API
    $oscrApiResponse = array();
    try {
      $oscrApiResponse = $client->prepareRequest('demographic', [ $perPage, $pageNum, $updatedSince], 'getDemographics')->request();
    } catch (Exception $e) {
      throw $e;
    }
    
    // Load up the right stuff
    $oscrModel = EloquentModelFactory::make('demographic');

    // Save each returned demo to the db and send back in response
    $savedModels = array();
    $savedErrorsCount = 0;
    if ( !empty($oscrApiResponse) ) {
      
      foreach ($oscrApiResponse as $_demographic) {
        try {
          $savedModels[] = $oscrModel->saveOscrResponseItem( $_demographic );
        } catch (Exception $e) {
          $savedErrorsCount++;
        }
      }

    }
    
    $savedModelsCount = count($savedModels);
    $syncResponse = [
      'oscarhost_demographics'  => $savedModels,
      'total_saved'             => $savedModelsCount,
      'total_errors'            => $savedErrorsCount,
      'total_received'          => count( $oscrApiResponse )     
    ];

    return $syncResponse;
  }

  public function fetchDemographic( $demographicNo = 0 )
  {
    // Start a client and do a pull
    $client = $this->getOscrClient();

    // Request Demographic data from the OscarHost API
    $oscrApiResponse = array();
    try {
      $oscrApiResponse = $client->prepareRequest('demographic', [$demographicNo], 'getDemographic')->request();
    } catch (Exception $e) {
      throw $e;
    }

    //If an empty response is returned, handle it.
    if ( empty($oscrApiResponse) ) {
      $syncResponse = [
        'oscarhost_demographics'  => 0,
        'total_saved'             => 0,
        'total_errors'            => 0,
        'total_received'          => $oscrApiResponse     
      ];

      return $syncResponse;
    }

    // Update the db with the demographic data
    $oscrModel = EloquentModelFactory::make('demographic');
    try {
      $oscrModel = $oscrModel->saveOscrResponseItem( (object) $oscrApiResponse );
    } catch (Exception $e) {
      throw $e;
    }

    // Pull the medical document data from oscar and save to the db
    $medDocResponse = $this->fetchEformsMeddocsByDemographic($oscrModel->oscr_demographicNo);

    // Load the relations from the db
    // @todo after building OscarhostDemographic->loadMedicalDocuments
    // $oscrModel->load('OscarhostEformKVDatum');

    // All med docs + demographic
    $total_saved = $medDocResponse['total_saved'] + 1;
    $total_errors = $medDocResponse['total_errors'];
    $total_received = $medDocResponse['total_received'] + 1;
    $syncResponse = [
      'oscarhost_demographics'  => $oscrModel,
      'total_saved'             => $total_saved,
      'total_errors'            => $total_errors,
      'total_received'          => $total_received 
    ];

    return $syncResponse;
  }

  public function fetchEformsAll()
  {

    // Start a client and do a pull
    $client = $this->getOscrClient();

    // Request data from the OscarHost API
    $oscrApiResponse = array();
    try {
      $oscrApiResponse = $client->prepareRequest('eform', [], "getEFormList")->request();
    } catch (Exception $e) {
      throw $e;
    }

    // Load up the right stuff
    $oscrModel = EloquentModelFactory::make('eform');

    // Save each returned demo to the db and send back in response
    $savedModels = array();
    $savedErrorsCount = 0;
    if ( !empty($oscrApiResponse) ) {
      
      foreach ($oscrApiResponse as $_eform) {
        try {
          $savedModels[] = $oscrModel->saveOscrResponseItem( $_eform );
        } catch (Exception $e) {
          $savedErrorsCount++;
        }
      }

    }
    
    $savedModelsCount = count($savedModels);
    $syncResponse = [
      'oscarhost_eforms'        => $savedModels,
      'total_saved'             => $savedModelsCount,
      'total_errors'            => $savedErrorsCount,
      'total_received'          => count( $oscrApiResponse )     
    ];

    return $syncResponse;
  }

  public function fetchEformsMeddocsByDemographic( $demographicNo = 0, $latest = false )
  {

    $client = $this->getOscrClient();
    
    // Get the id for the medical document eform
    $_eform = EloquentModelFactory::make('eform');
    $_eform = $_eform::where('oscr_formName', 'Medical Document')->firstOrFail();
    
    // $_externalId = "oscr_id"
    $_externalId = $_eform->externalId;

    // $_externalId = 123
    $_externalId = $_eform->$_externalId;

    // Request data from the OscarHost API
    $eformDataList = array();
    $eformValueList = array();
    try {

      $_call['method'] = "getEFormDataList";
      $_call['args'] = [ (int) $_externalId, $demographicNo ];
      
      if ($latest) {
        $_call['method'] = "getEFormDataListFiltered";
        $_call['args'] = [
          (int) $_externalId, $demographicNo,
          [
            [ 
              'key'     =>  'start_date',
              'value'   =>  date( "Y-m-d", strtotime("2 years ago") ) 
            ],
            [ 
              'key'     =>  'end_date',
              'value'   =>  date( "Y-m-d" ) 
            ],
            [ 
              'key'     =>  'page',
              'value'   =>  1
            ],
            [ 
              'key'     =>  'items_per_page',
              'value'   =>  100 
            ]
          ]
        ];  
      }

      // Pull EformDataList. This is an array of small objects for each medical document for this patient
      // We'll need the id value to pull the EformValueList which returns an array of objects containing keys and values about the eform
      $eformDataList = $client->prepareRequest('eform', $_call['args'], $_call['method'])->request();

    } catch (Exception $e) {
      throw $e;
    }
      
    // Go through each eformDataList and store the values in $eformValueList
    $_total_received = 0;
    foreach ($eformDataList as $value) {
      
      try {
        $processedValue = $this->processEformDataList($value);
      } catch (Exception $e) {
        throw $e;
      }

      if ( !isset($processedValue['success']) || $processedValue['success'] !== true ) {
        continue;
      }
      
      if ( !isset($processedValue['eformValueItem']) || empty($processedValue['eformValueItem'])) {
        continue;
      }
      
      // Add to array for storage below
      $eformValueList[] = $processedValue['eformValueItem'];
      $_total_received++;
      
    }

    // Save models
    // - I know I can optimize by doing batch inserts
    // - But then I don't get the validation that comes with Eloquent Models
    $savedModels = array();
    $savedErrorsCount = 0;
    $oscrModel = EloquentModelFactory::make('eformData');
    foreach ($eformValueList as $_eformMeddoc) {

      try {
        $oscrModel->saveOscrResponseItem( $_eformMeddoc );
      } catch (Exception $e) {
        $savedErrorsCount++;
      }

    }

    // Build the KV rows into documents
    $_efRepository = new EformRepository($oscrModel);
    $medicalDocuments = $_efRepository->buildMedDocsFromKVData( $demographicNo );
    
    // Return response
    $savedModelsCount = count($savedModels);
    $syncResponse = [
      'medical_documents'       => $medicalDocuments,
      'total_saved'             => $savedModelsCount,
      'total_errors'            => $savedErrorsCount,
      'total_received'          => $_total_received    
    ];

    return $syncResponse;
  }

  /**
   * This method takes an eformDataList item as part of the response from the oscarhost api
   * and creates an object that is ready to be written by the EformKVDatum model
   * @param  obj $eformDataListItem   Individual elements from the array returned by getEFormDataList and getEFormDataListFiltered
   * @return array                    [ 'success' => (bool), eformValueItem => (object) ]
   * @throws Exception                rethrows the exception for getEformValueList call
   */
  protected function processEformDataList( $eformDataListItem )
  {

    $client = $this->getOscrClient();

    $processResponse = [
      'eformValueItem'  =>  null,
      'success'         =>  false
    ];

    if ( !is_object($eformDataListItem) || !property_exists($eformDataListItem, 'id') ) {
      return $processResponse;
    }

    // Get the EformValueList from the eform
    $_efval = array();
    $_response = null;
    try {
      $_response = $client->prepareRequest('eform', [$eformDataListItem->id], "getEFormValueList")->request();
    } catch (Exception $e) {
      throw $e;
    }

    // Create the object for the eformValueItem
    if ( !empty($_response) ) {

      // Build an keyed array with the data in EformValueList
      foreach ($_response as $_val) {
        if ( property_exists($_val, 'varName') && property_exists($_val, 'varValue')) {
          $_efval[$_val->varName] = $_val->varValue;
        }
      }

      // Merge the eformData and eformValue (we save properties from both objects into the db as key/vals)
      if ( !empty($_efval) ) {
        $eformDataListItem = (array) $eformDataListItem;
        $_eformValueItem = array_merge($eformDataListItem, $_efval);
        $processResponse['success'] = true;
        $processResponse['eformValueItem'] = (object) $_eformValueItem;
      }
    }

    return $processResponse;
  }
}