<?php

namespace App\Http\Controllers\api\v1;

use App\Emr\OscarHost\Sync as OscarSync;
use App\Emr\Api\Responder;

use App\Http\Controllers\Controller;
use Exception;

class OscarHostClientController extends Controller
{
  protected $responder;

  /**
   * Load the Emr api responder class for responses
   * @param Responder $responder                  Application specific class for api responses. Responses are always in the following format:
   * {
   *   'status': 'success|error',
   *   'message': "String explaining what's happening"
   *   'data': { some: data }
   * }
   * Responses also have an HTTP code (usually 200)
   */
  function __construct(Responder $responder)
  {
    $this->responder = $responder;
  }

  public function fetchDemographicList($pageNum = 1, $perPage = 100, $updatedSince = '2014-01-01')
  {
    $oscarSync = new OscarSync();

    try {
      $results = $oscarSync->fetchDemographicsByPage($pageNum, $perPage, $updatedSince);
    } catch (Exception $e) {
      return $this->responder->respond('error', $e->getMessage(), ['resource' => 'demographic'], 400);
    }

    return $this->responder->respond('success', $results['total_saved']." records inserted/updated", $results, 200);
  }

  public function fetchDemographic($demographicNo = 0)
  {
    $oscarSync = new OscarSync();

    try {
      $results = $oscarSync->fetchDemographic($demographicNo);
    } catch (Exception $e) {
      return $this->responder->respond('error', $e->getMessage(), ['resource' => 'demographic'], 400);
    }

    return $this->responder->respond('success', $results['total_saved']." records inserted/updated", $results, 200);
  }

  public function fetchEformList() 
  {
    $oscarSync = new OscarSync();

    try {
      $results = $oscarSync->fetchEformsAll();
    } catch (Exception $e) {
      return $this->responder->respond('error', $e->getMessage(), ['resource' => 'eform'], 400);
    }

    return $this->responder->respond('success', $results['total_saved']." records inserted/updated", $results, 200);
  }

  public function fetchEformMeddocList( $demographicNo = 0, $latest = false )
  {
    $oscarSync = new OscarSync();

    $latest = ( $latest === false ) ? false : true ;

    try {
      $results = $oscarSync->fetchEformsMeddocsByDemographic($demographicNo, $latest);
    } catch (Exception $e) {
      return $this->responder->respond('error', $e->getMessage(), ['resource' => 'eformMeddoc'], 400);
    }

    return $this->responder->respond('success', $results['total_saved']." records inserted/updated", $results, 200);
    
  }
}