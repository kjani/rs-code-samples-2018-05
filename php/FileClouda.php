<?php 

use GuzzleHttp\Client;

/**
* Handles the CRUD of files on clouda's openstack bulk storage.
* Each instance represents 1 file
*/
class FilePatientFormClouda implements FileInterface {

  public $container;
  public $client;
  public $filedata = null;
  public $fileid = null;
  public $filerow = array();
  public $storage_type = 'clouda';
  
  function __construct( $file_id = null ) {

    // Load CI and any required dependencies
    $this->ci =& get_instance();
    $this->ci->load->model('file_model');
    $this->ci->load->model('user_file_model');

    // Load name of the clouda container being used
    $this->container = getenv('REG_FORM_CLOUDA_CONTAINER');

    // Load up guzzle client with clouda specific info
    $this->client = new Client([
        'base_uri' => getenv('CLOUDA_OBJECT_STORE_ENDPOINT'),
        'timeout'  => 15.0,
        'headers' => array(
          'X-Container-Meta-Full-Key' => getenv('CLOUDA_PATIENT_REG_FORMS_CONTAINER_ACCESS_KEY')
        )
    ]);

    if ( !empty( $file_id ) ) {
      $this->load( $file_id );
    }
  }

  /**
   * Set the filedata property for the class
   * @param string $filedata the file as a string. ex: the fgetcontents() output for the file
   */
  public function set_filedata( $filedata ){
    $this->filedata = $filedata;
  }

  /**
   * Get the filedata property for the class
   * @return  string representation of file as a string
   */
  public function get_filedata() {
    return $this->filedata;
  }

  
  /**
   * Write the file to clouda and db
   * @param  string $filepath the full path where the file will be stored
   * @param  string $filename the name with extention
   * @param  string $mime the mimetype of the file
   * @return void
   */
  public function write( $filepath, $filename, $mime ){

    if ( !isset( $this->filedata ) || empty( $this->filedata ) ) {
      throw new Exception("No filedata set.", 1);
    }

    if ( empty( $filepath ) || empty( $filename ) || empty( $mime ) ) {
      throw new Exception("All arguments are required. Please make sure you have supplied a filepath, filename, and mime.", 1);
      
    }

    // Add leading slash to filepath if none exists
    if ( substr( $filepath, 0, 1 ) != '/' ) {
      $filepath = '/' . $filepath;
    }

    // Add trailing slash to filepath if none exists
    if ( substr( $filepath, -1 ) != '/') {
      $filepath .= '/';
    }

    // Send to Clouda
    $request_options = array(
      'body' => $this->filedata
    );


    try {
      $response = $this->client->request('PUT', $this->container . $filepath . $filename, $request_options);
      $_success = true;
    } catch (Exception $e) {
      throw $e;
    }

    $status_code = $response->getStatusCode();
    if ( $status_code != 201) {
      throw new Exception("Invalid status code returned from CloudA. Status Code: " . $status_code, 1);
    }

    // Write row to files table
    // Insert if new, update if existing
    if ( empty( $this->fileid ) || empty( $this->filerow ) ) {

      $user = $this->ci->ion_auth->user();

      $userid = ( isset( $user->id ) ) ? $user->id : null ;

      $row_data = array(
        'user_id'       => $userid,
        'name'          => $filename,
        'description'   => null,
        'mime'          => $mime,
        'storage_type'  => $this->storage_type,
        'path'          => $filepath . $filename,
        'deleted'       => 0
      );

      $file_id = $this->ci->file_model->insert( $row_data );
      
      // set fileid and filerow properties
      $this->fileid = $file_id;
      $this->filerow = $this->ci->file_model->get( $this->fileid );



    } else {
      
      $row_data = $this->filerow;

      if ( isset( $row_data['id'] ) ) {
        unset( $row_data['id'] );
      }

      $row_data['name'] = $filename;
      $row_data['path'] = $filepath . $filename;
      $row_data['mime'] = $mime;

      $this->ci->file_model->update( $this->fileid, $row_data );
    }

    return true;

  }


  /**
   * Load the file from the db and storage system
   * @param  string/int $file_id the id for the file in the files table
   * @return void
   */
  public function load( $file_id = null ){

    if ( empty( $file_id ) || !ctype_digit( (string) $file_id ) ) {
      throw new Exception("A file id is required to load a file.", 1);
    }

    // query the db for the file
    $this->filerow = $this->ci->file_model->get( $file_id );

    if ( empty( $this->filerow ) ) {
      throw new Exception("No file found in the database with id: " . $file_id, 1);
    }

    // Make sure file is stored on clouda
    if ( $this->filerow['storage_type'] != $this->storage_type) {
      throw new Exception("The file requested is not stored on CloudA.");
    }

    $this->fileid = $file_id;

    // Download the file from clouda and load it into the
    $response = $this->client->request('GET', $this->container . $this->filerow['path'] );
    $status_code = $response->getStatusCode();

    if ( $status_code != 200 ) {
      throw new Exception("Incorrect status code returned from CloudA. Status Code: " . $status_code, 1);
    }

    // Load filedata into property
    $this->set_filedata( $response->getBody() );

  }

  /**
   * Delete the file from the storage system and db
   * @param  string/int $file_id the id for the file in the files table
   * @return void
   */
  public function delete( $file_id = null ){

    if ( !empty( $this->fileid ) ) {
      $file_id = $this->fileid;
    }

    if ( empty( $file_id ) || !ctype_digit( (string) $file_id ) ) {
      throw new Exception("A file id is required to load a file.", 1);
    }

    // query the db for the file
    $this->filerow = $this->ci->file_model->get( $file_id );

    if ( empty( $this->filerow ) ) {
      throw new Exception("No file found in the database with id: " . $file_id, 1);
    }

    // Make sure file is stored on clouda
    if ( $this->filerow['storage_type'] != $this->storage_type) {
      throw new Exception("The file requested is not stored on CloudA.");
    }

    // soft delete the file in the db
    $this->ci->file_model->delete( $file_id );

    // Delete the file from clouda
    $response = $this->client->request('DELETE', $this->container . $this->filerow['path']);
  }
}