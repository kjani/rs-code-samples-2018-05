<?php 

use Valitron\Validator;
use GuzzleHttp\Client;

/**
* The class responsible for sending api calls to the SR Fax API
*/
class FaxSRFax implements FaxInterface
{

	// SR Fax
	const apiBaseuri = "https://www.srfax.com/SRF_SecWebSvc.php";
	// - Required info for API calls.
	public $srAccessId;
	public $srEmail;
	private $srPwd;
	private $srFaxNum;
	private $srFaxType = "SINGLE"; // SINGLE or BROADCAST

	// Class specific
	const serviceName = "srfax";
	public $callParams = array(); 
	protected $lastResponse = null;
	protected $lastServiceId = null;
	
	function __construct() {

		$this->srAccessId = getenv("SRFAX_ACCESS_ID");
		$this->srPwd = getenv("SRFAX_PWD");
		$this->srFaxNum = getenv("SRFAX_FAXNUM");
		$this->srEmail = getenv("SRFAX_EMAIL");

		// Make sure we have non-empty props
		try {
			$this->validateEnvVars();	
			$this->buildCallParams();
		} catch (Exception $e) {
			throw $e;
		}	
	}

	// FaxInterface methods
	/**
	 * Send a fax
	 * @param  array 	$files 	array where each element is a FileInterface implementation
	 * @return GuzzleHttp\Psr7\Response
	 */
	public function send( $files = array(), $faxNum ) {

		// @todo update later. add namespaces to libraries/canvasrx, check that each obj implements FileInterface
		// validate inputs
		// - files
		foreach ($files as $_file) {
			if ( !method_exists($_file, 'get_filedata') ) {
				throw new Exception("FaxSRFax - sendFax - invalid files passed in argument", 1);
			}
		}

		// - faxnum. very unsophisticated dumbness.
		$v = new Validator(array('faxNum'	=> $faxNum));
		$rules = array(
			'faxNum'	=>	['integer', ['max', 19999999999]]
		);
		if ( !$v->validate() ) {
			throw new Exception("FaxSRFax - sendFax - invalid faxNum", 1);
		}

		// build request params
		$params = array(
			'action'	=>	"Queue_Fax",
			'sCallerID'	=>	$this->srFaxNum,
			'sSenderEmail'	=>	$this->srEmail,
			'sFaxType'	=>	$this->srFaxType,
			'sToFaxNumber'	=>	$faxNum,
			'sRetries'	=>	6
		);

		// - add params with file data
		$fileParams = array();
		$i = 1;
		foreach ($files as $_file) {
			$_fileNameKey = 'sFileName_' . $i;
			$_fileContentKey = 'sFileContent_' . $i;
			$_fileContent = base64_encode( $_file->get_filedata() );
			
			$fileParams[$_fileNameKey] = 'fax{$i}.pdf';
			$fileParams[$_fileContentKey]	=	$_fileContent;

			$i++;
		}

		// Make the request
		try {
			$params = array_merge( $params, $fileParams );
			$this->buildCallParams( $params );
			$response = $this->call();
		} catch (Exception $e) {
			throw $e;
		}

		// Store the service id in lastServiceId
		$_respArr = json_decode($response->getBody(), true);
		$this->lastServiceId = $_respArr['Result'];

		return $response;
	}

	/**
	 * Get the statuses from SRFax for faxes
	 * @param  array  $serviceIds array where each element is a service id for SRFax. Usually corresponds to faxes.service_id
	 * @return array             Format: [ {serviceId1}	=>	{status1},	{serviceId2}	=>	{status2}, ...]
	 */
	public function getStatus( $serviceIds = array() ) {

		// Validate args
		try {
			$this->validateServiceIds($serviceIds);
		} catch (Exception $e) {
			throw $e;
		}

		// Build the params for the api call
		$params = array();
		if ( is_scalar($serviceIds) ) {
			$params['action'] = "Get_FaxStatus";
			$params['sFaxDetailsID'] = $serviceIds;
		} elseif ( is_array($serviceIds) ) {
			$params['action'] = "Get_MultiFaxStatus";
			$params['sFaxDetailsID'] = implode('|', $serviceIds);
		}

		// Make the request
		try {
			$this->buildCallParams( $params );
			$response = $this->call();
		} catch (Exception $e) {
			throw $e;
		}
				
		// Parse the response to get crx specific statuses
		$_respObj = json_decode( $response->getBody() );
		$respResult = $_respObj->Result;
		$respResult = ( !is_array($respResult) ) ? array($respResult) : $respResult;
		$serviceIds = ( !is_array($serviceIds) ) ? array($serviceIds) : $serviceIds;
		$statuses = $this->parseSRFaxStatusResponse( $respResult, $serviceIds );

		// return
		// $statuses format: [ [{service_id1} 	=>	'sent'], [{service_id2} 	=>	'sent']]
		return $statuses;
	}

	/**
	 * Get the service name, stored in a class constant.
	 * @return string The name of the service (in this case 'srfax')
	 */
	public function serviceName() {
		return self::serviceName;
	}

	/**
	 * Get the current lastServiceId property
	 * @return int the service id returned from the last sent fax
	 */
	public function serviceId() {
		return $this->lastServiceId;
	}

	/**
	 * Get the response for the most recent API call
	 * @return GuzzleHttp\Psr7\Response Guzzle response object
	 */
	public function response() {
		return $this->lastResponse;
	}

	// Class specific methods
	// - API Calls
	/**
	 * Build the parameters for the request
	 * @param  array  $params 	array where keys are param names and values are param values
	 * @return void 
	 */
	public function buildCallParams( $params = array() ) {
		
		// Validate arg
		if ( !is_array( $params ) ) {
			throw new Exception("FaxSRFax - buildCallParams - invalid params passed in input", 1);
		}

		// Set defaults
		$callParams = $this->callParams;
		$callParams['action']			= "";
		$callParams['access_id']	=	$this->srAccessId;
		$callParams['access_pwd']	=	$this->srPwd;

		// Merge other params passed via argument
		$callParams = array_merge( $callParams, $params );
		
		$this->callParams = $callParams;
	}

	/**
	 * Perform the API request
	 * @return GuzzleHttp\Psr7\Response
	 */
	protected function call() {

		// Load up a new Guzzle client
		$client = new Client([
			'base_uri'	=>	self::apiBaseuri,
			'timeout'		=>	25
		]
		);

		// Limit requests to sending and checking statuses
		if ( !in_array( $this->callParams['action'], self::getValidApiCalls() ) ) {
			throw new Exception("FaxSRFax - call - Invalid action set for api call", 1);
		}

		// Perform request
		try {
			$response = $client->request('POST', '', [
				'form_params'	=> $this->callParams
			]);
		} catch (Exception $e) {
			throw $e;
		}

		// Non 200 status code, throw exception
		if ( $response->getStatusCode() != 200 ) {
			$body = (string) $response->getBody();
			throw new Exception("FaxSRFax - call - api response error: " . $body, 1);
		}

		// Make sure it's successful. The SRFax api will have a property called Status whose value is Success
		$_responseJson = (string) $response->getBody();
		$_responseArr	=	json_decode($_responseJson, true);
		if ( !isset($_responseArr['Status']) || $_responseArr['Status'] != "Success")  {
			throw new Exception("FaxSRFax - call - unsuccessful response: " . $_responseJson, 1);
		}

		// Store the response in the lastResponse prop for later use
		$this->lastResponse = $response;

		return $response;
	}

	/**
	 * Method used getStatus to build an array of statuses by service id
	 * @param  array  $responseResult An array of Result objects returned from the request
	 * @param  array  $serviceIds     Service ids whose status should be in responseResult
	 * @return array                  Fax statuses by service id. Format: [ [{service_id1} 	=>	'sent'], [{service_id2} 	=>	'sent']]
	 */
	private function parseSRFaxStatusResponse( $responseResult = array(), $serviceIds = array() ) {

		// Create an array where the keys are the serviceIds and the values are corresponding crx statuses
		// use null for default values
		$statusByServiceId = array_flip($serviceIds);
		foreach ($statusByServiceId as $key => $value) {
			$statusByServiceId[ (int) $key]	= null;
		}
		
		// Go through each status object in the response. Parse the service id from the FileName property
		// Set a crx specific status for service ids in $statusByServiceId
		foreach ($responseResult as $key => $resultObj) {
			
			$respServiceId = "";
			if ( property_exists($resultObj, 'FileName') && strpos($resultObj->FileName, "|") !== false ) {
				$_id = explode('|', $resultObj->FileName);
				$respServiceId = $_id[1];
			}

			$respStatus = ( property_exists($resultObj, 'SentStatus') ) ? $resultObj->SentStatus : "" ;

			if ( array_key_exists( (int) $respServiceId, $statusByServiceId ) ) {
				$statusByServiceId[$respServiceId] = $this->getMappedCrxStatus($respStatus);
			}
		}

		return $statusByServiceId;
	}

	/**
	 * Get the CRX specifc status name based on the SRFax status
	 * @param  string $status SRFax status of fax
	 * @return string         queued|sent|failed|other
	 */
	private function getMappedCrxStatus( $status = "" ) {
		if ( !is_string($status) ) {
			throw new Exception("FaxSRFax - getMappedCrxStatus - Invalid status type", 1);
		}

		$srFaxToCrxStatuses = array(
			'In Progress'	=>	'queued',
			'Sent'	=>	'sent',
			'Failed'	=>	'failed'
		);

		if ( isset( $srFaxToCrxStatuses[$status] ) ) {
			return $srFaxToCrxStatuses[$status];
		}

		return 'other';
	}

	// - Validation
	/**
	 * Used to validate the StatusIds before making an api call
	 * @param  mixed 	$serviceIds 	a single or an array of values that correspond to faxes.service_id in the db
	 * @throws Exception
	 * @todo use Valitron
	 * @return void
	 */
	private function validateServiceIds( $serviceIds ) {
		
		// check for valid types
		if ( !is_scalar($serviceIds) && !is_array($serviceIds) ) {
			throw new Exception("FaxSRFax - validateServiceIds - statusId must be a single or array of ids", 1);
		}
		
		// validate $statusId
		$statusType = 'single';
		
		// - single
		if ( is_scalar($serviceIds) ) {
			if ( !ctype_digit( (string) $serviceIds ) || $serviceIds < 1 ) {
				$_type = gettype($serviceIds);
				throw new Exception("FaxSRFax - validateServiceIds - invalid statusId. type: {$_type}, val: {$statusIds}", 1);
			}
		}

		// - multiple
		if ( is_array($serviceIds) ) {
			
			$statusType = 'multiple';

			if ( empty($serviceIds) ) {
				throw new Exception("FaxSRFax - validateServiceIds - statusIds cannot be empty", 1);
			}

			foreach ($serviceIds as $_sid) {
				if ( !ctype_digit( (string) $_sid ) ) {
					throw new Exception("FaxSRFax - validateServiceIds - all statusIds must be numeric", 1);
				}
			}
		}
	}

	/**
	 * Limit the types of calls the object can make to SRFax
	 * @return string SRFax name of the API calls
	 */
	public static function getValidApiCalls() {
		return array('Queue_Fax', 'Get_FaxStatus', 'Get_MultiFaxStatus');
	}

	/**
	 * Ensure that env vars required for this class exist
	 * @return void
	 */
	private function validateEnvVars() {
		
		$data = [
			'srAccessId'	=>	$this->srAccessId,
			'srPwd'				=>	$this->srPwd,
			'srFaxNum'		=>	$this->srFaxNum,
			'srEmail'			=>	$this->srEmail
		];
		$v = new Validator($data);

		$rules = array(
			'required'	=>	array('srAccessId', 'srPwd', 'srFaxNum', 'srEmail'),
			'lengthMin'	=>	array(
				array('srAccessId', 1),
				array('srPwd', 1),
				array('srFaxNum', 1),
				array('srEmail', 1),
			),
			'integer'		=>	array('srFaxNum'),
			'length'		=>	array( array('srFaxNum', 10) )
		);
		$v->rules($rules);

		if ( !$v->validate() ) {
			throw new Exception("FaxSrFax - validateEnvVars - " . json_encode($v->errors()), 1);
		}
	}
}