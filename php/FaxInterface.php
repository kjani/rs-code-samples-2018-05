<?php 

interface FaxInterface {
	
	public function send( $files, $faxNum );
	public function getStatus( $serviceIds );
	public function serviceName();
	public function serviceId();
	public function response();
	
}