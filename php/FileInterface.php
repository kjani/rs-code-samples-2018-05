<?php

interface FileInterface {

  /**
   * Set the filedata property for the class
   * @param string $filedata the file as a string. ex: the fgetcontents() output for the file
   */
  public function set_filedata( $filedata );

  /**
   * Get the filedata property for the class
   * @return  string representation of file as a string
   */
  public function get_filedata();
  
  /**
   * Write the file to the storage system and db
   * @param  string $filepath the full path where the file will be stored
   * @param  string $filename the name with extention
   * @param  string $mime the mimetype of the file
   * @return void
   */
  public function write( $filepath, $filename, $mime );

  /**
   * Load the file from the db and storage system
   * @param  string/int $file_id the id for the file in the files table
   * @return void
   */
  public function load( $file_id );

  /**
   * Delete the file from the storage system and db
   * @param  string/int $file_id the id for the file in the files table
   * @return void
   */
  public function delete( $file_id );

}