<div class="container admin-page">
  <div class="row">
    <div class="col-sm-3">
      <?php $this->load->view('admin/templates/sidebar'); ?>
    </div>
    
    <div class="col-sm-9">
    
      <div>
        <?php $this->load->view('admin/templates/breadcrumb'); ?>
      </div>
      
      <?php if (isset($msg)): ?>
        <div class="alert alert-info"><?php echo $msg; ?></div>
      <?php endif; ?>

      <?php if ( $passcode_expires): ?>
        <p>
          <span class="glyphicon glyphicon-exclamation-sign"></span> 
          The passcode expires at <?php echo date( "g:i a", strtotime($passcode_expires) ); ?>
        </p>
      <?php endif ?>


      <h3>Reconcile Registrations</h3>
      
      <p>Use the form below to upload a reconciliation csv file that has been completed by the vendor.</p>
            
      <form action="<?php echo site_url('admin/reconciliation/upload_and_process'); ?>" method="post" class="form-horizontal" name="admin_reg_csv_upload" accept-charset="utf-8" enctype="multipart/form-data">
      
        <div class="form-group">
          <?php if(isset($err_msg) and $err_msg != '') : ?>
            <div class="col-sm-9">
              <div class="alert alert-danger">
                <?php echo $err_msg; ?>
              </div>
            </div>
          <?php endif; ?>
        </div>

        <div class="form-group">
          <div class="col-sm-9">
            <input type="file" id="userfile" name="userfile" accept=".csv">
          </div>
        </div>

        <div class="form-group">
            <div class="message alert alert-danger" role="alert" style="display:none;"></div>
        </div>

        <div class="form-group">
          <div class="col-sm-9">
            <button type="submit" class="btn btn-info pull-left" id="upload-csv">Upload + Process CSV</button>
            <img src="<?php echo site_url('assets/images/ajax-loader.gif') ?>" class="ajax-loader" style="display: none">
          </div>
        </div>

        <div class="form-group" id="results" style="display:none;">
          <div class="col-sm-9">
            <span id="upload-status"></span><br>
            <span id="success-registrations"></span><br>
            <span id="skipped-registrations"></span><br>
            <span id="errors-registrations"></span><br>
            <span id="results-download"></span>
          </div>
        </div>      
      </form>

      <hr>

      <div class="row">
        <div class="col-sm-9">
          <h4>Open Registrations</h4>
          <?php if ( count($registration_statuses) ): ?>
            <p>Valid registration statuses: </p>
            <ul>
              <?php foreach ($registration_statuses as $_status): ?>
                <li>
                  <code>
                    <?php echo $_status['name']; ?>
                  </code>
                  <?php if ( $_status['completed'] == 1 ) { echo " - registration set as complete"; }  ?>
                </li>  
              <?php endforeach ?>
            </ul>
          <?php endif ?>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-9">
          <div class="table-responsive">
            <table class="table table-striped table-hover">
              <thead>
              <tr>
                <th width="4%">Vendor</th>
                <th width="3%">Open Registrations</th>
                <th width="4%">CSV</th>
              </tr>
              </thead>
              <tbody>
                <?php foreach($open_registrations as $producer_id => $reg_count) : ?>
                  <tr>
                    <td><?php echo $producers[$producer_id]['name']; ?></td>
                    <td><?php echo $reg_count; ?></td>
                    <td><a href="<?php echo site_url('_admin/reconciliation/generate_open_reg_csv/'.$producer_id); ?>" target="_blank">Generate CSV</a></td>
                  </tr>
                <?php endforeach; ?>
              </tbody>
            </table>
          </div>
        </div>
      </div>
  </div>
</div>

<script type="text/javascript" charset="utf-8">
  var upload_url = "<?php echo site_url('/admin/reconciliation/upload_and_process'); ?>";
  var csv_download_url = "<?php echo site_url('admin/files/download') ?>";
</script>