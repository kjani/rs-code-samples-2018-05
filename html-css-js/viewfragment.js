  $(function() {

	// Upload file
	$("form[name=admin_reg_csv_upload]").on('submit', function(event) {
		event.preventDefault();

        var formMessageDiv = $(this).find(".message");
        var resultsDiv = $(this).find("#results");
        var ajaxLoader = $(this).find(".ajax-loader")
		var form_action = $(this).attr('action');
        var _file = $(this).find("#userfile");
        var results = {
            upload_status: $(this).find("#upload-status"),
            success_registrations: $(this).find("#success-registrations"),
            skipped_registrations: $(this).find("#skipped-registrations"),
            errors_registrations: $(this).find("#errors-registrations"),
            results_download: $(this).find("#results-download"),
        };
        
        formMessageDiv.html("");
        formMessageDiv.hide();
        resultsDiv.hide();
        ajaxLoader.show();

    
        // is a file selected for uploading?
        if(_file.val() === "") {
            formMessageDiv.html("Please choose a csv file before uploading.");
            ajaxLoader.hide();
            return false;
        }

        var files = _file[0]['files'];
  		var data = new FormData();

  		$.each(files, function(key, value) {
  			data.append('userfile', value);
  		});
        
        $.ajax({
            url: form_action,
            type: 'POST',
            dataType: 'json',
            data: data,
            cache: false,
            processData: false, // Don't process the files
            contentType: false, // Set content type to false as jQuery will tell the server its a query string request
        })
        .done(function(response) {
            
            if ( response.hasOwnProperty('error') ) {
                formMessageDiv.show().html(response.error);
                return false;
            }

            if ( response.hasOwnProperty('status') === false ) {
                formMessageDiv.show().html("There was an error uploading and processing the file. Please contact the engineering+bugs@client to report this bug.");
                console.log( response );
                return false;   
            }

            resultsDiv.show();
            results.upload_status.html(response.message);
            results.success_registrations.html(response.data.success + " registration(s) processed.");
            results.skipped_registrations.html(response.data.skipped + " registration(s) skipped.");
            results.errors_registrations.html(response.data.errors + " registration(s) with errors.");
            results.results_download.html( 
                '<a href="' + 
                    csv_download_url + "/" + response.data.file_id + '" target="_blank">' + 
                    "Download processed csv" +
                '</a>'
            );
        })
        .fail(function(response) {
            formMessageDiv.show().html("There was an error uploading and processing the file. Please contact the engineering+bugs@client to report this bug.");
            console.log(response);
        })
        .always(function() {
            ajaxLoader.hide();
            _file.val(null);
        });
        
	});

 });