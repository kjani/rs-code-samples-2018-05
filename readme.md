**/php**

These classes are part of an application that stores files across multiple storage providers and sends faxes using API based faxing services

The interfaces, FileInterface.php and FaxInterface.php, provide the implementations for File and Fax classes for the different services.

Classes:

- FileClouda is a class to manage files on clouda.ca's openstack bulk storage. Files are indexed in a database table.
- FaxSRFax is a class for sending faxes and statuses using srfax.com's API.

**/laravel-specific**

The application is a private API built with Laravel Passport. The endpoints for OscarHostClientController pull data from an Electronic Medical Records provider and display the status of the pull.

The Sync class uses the Client class to make SOAP based calls to the EMR service (Oscar Host). Sync processes the data and writes/overwrites into the database.

**/html-css-js**

This is a view fragment for a form in an internal application where users upload csvs for parsing server side. The form is built with bootstrap and uses jQuery for DOM manipulation and an ajax call
